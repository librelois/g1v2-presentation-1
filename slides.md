---
theme: default
class: text-center
highlighter: shiki
lineNumbers: false
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
drawings:
  persist: false
css: unocss
title: "Ğ1 v2.0: Ce qui va changer"
---

# Ğ1 v2.0 : Ce qui va changer

Éloïs SANCHEZ - 14 Juillet 2022

<!--
The last comment block of each slide will be treated as slide notes. It will be visible and editable in Presenter Mode along with the slide. [Read more in the docs](https://sli.dev/guide/syntax.html#notes)
-->

---
hideInToc: true
---

# Sommaire

<Toc columns="2" />


<!--
You can have `style` tag in markdown to override the style for the current page.
Learn more: https://sli.dev/guide/syntax#embedded-styles
-->

<style>
h1 {
  background-color: #2B90B6;
  background-image: linear-gradient(45deg, #4EC5D4 10%, #146b8c 20%);
  background-size: 100%;
  -webkit-background-clip: text;
  -moz-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-text-fill-color: transparent;
}
</style>

---
layout: two-cols
---

# La Ğ1 en apparence 

<img src="/img/Cesium-phone.png" class="m-5 h-30 rounded" />

<arrow x1="200" y1="160" x2="500" y2="160" color="#564" width="3" arrowSize="1" />


<div class="absolute top-220px">
Alice
</div>

<br>

<v-clicks>

* Comment contacter le tel/ordi de Bob ?
* Et si Bob n'est pas en ligne ?
* Où sont sauvegardées les informations ?
* ⇒ Cesium ne peut pas fonctionner tout seul
</v-clicks>

::right::

<img src="/img/Cesium-phone.png" class="absolute left-500px top-80px m-5 h-30 rounded" />

<div class="absolute top-220px">
Bob
</div>
 

[^1]: [Learn More](https://sli.dev/guide/syntax.html#line-highlighting)

<style>
.footnotes-sep {
  @apply mt-20 opacity-10;
}
.footnotes {
  @apply text-sm opacity-75;
}
.footnote-backref {
  display: none;
}
</style>

---

# La Ğ1 en réalité

<img src="/img/Cesium-phone.png" class="m-5 h-30 rounded" />

<div class="absolute top-220px">
Alice
</div>

<arrow x1="200" y1="160" x2="320" y2="160" color="#f00" width="2" arrowSize="1" />

<img v-click src="/img/document-1.png" class="absolute left-250px top-80px h-20 rounded" />

<img src="/img/Duniter.png" class="absolute left-300px top-80px m-5 h-30 rounded" />

<arrow x1="450" y1="150" x2="550" y2="150" color="#564" width="4" arrowSize="1" />
<arrow x2="450" y1="170" x1="550" y2="170" color="#564" width="4" arrowSize="1" />

<img src="/img/Duniter.png" class="absolute left-540px top-80px m-5 h-30 rounded" />

<arrow x1="380" y1="225" x2="440" y2="310" color="#564" width="4" arrowSize="1" />
<arrow x2="400" y2="225" x1="450" y1="290" color="#564" width="4" arrowSize="1" />

<img src="/img/Duniter.png" class="absolute left-425px top-240px m-5 h-30 rounded" />


<arrow x1="610" y1="225" x2="570" y2="310" color="#564" width="4" arrowSize="1" />
<arrow x2="580" y2="220" x1="550" y1="280" color="#564" width="4" arrowSize="1" />


<arrow x1="730" y1="330" x2="570" y2="330" color="#f00" width="2" arrowSize="1" />

<img src="/img/Cesium-phone.png" class="absolute left-700px m-5 h-30 rounded" />

<div class="absolute top-380px left-750px">
Bob
</div>


---
class: px-20
---

# Pourquoi arrêter Duniter v1 ?

* Plus en développement actif, plus de nouvelle fonctionnalité depuis 4 ans, en "maintenance minimale".
* Déjà des problèmes de charge (alors que 6000 utilisateurs c'est rien).
* Designé à 90% par cgeek entre 2014 et 2017
  - Il n'avait pas de compétence spécifique blockchain 
  - Il n'y avait pas d'utilisateurs, seuls quelques dizaines de geek 🤓.
 * Codé de manière trop spécifique, pas assez flexible/évolutif ⇒ trop difficile à faire évoluer.
 
 <br>
 
 <img src="/img/maison-fissure.jpg" class="absolute left-300px m-5 h-50 rounded" />


---
class: px-20
---

# Les objectifs de Duniter v2

1. Pérenniser la Ğ1 (pour qu'elle ne s'arrête pas).
1. Réduire notre travail de développement à ce qui est spécifique à la monnaie libre, déléguer le reste à un « framework blockchain ».
1. Corriger la plupart des problèmes de Duniter v1 (notamment la synchro des « piscines »).
1. Ajouter de nouvelles fonctionnalités utiles et demandées depuis des années (comme le prélèvement automatique)
1. Intégrer des mécanismes de prises de décision directement dans la blockchain (demandera d’abord des longs échanges sur les modalités).

---
layout: two-cols
---

# Les nouvelles interfaces

  ## Ğecko

  <img src="/img/Gecko.png" class="m-5 h-30 rounded" />

* Développé par Étienne Bouché (poka)
* Mobile et Ordinateur (web) 
* Interface intuitive pour les nouveaux
* Projet de travailler avec un·e pro UX design

::right::

<h1 color="#fff">_</h1>

 ## Cesium v2

   <img src="/img/Cesium.png" class="m-5 h-30 rounded" />

* Développé par Benoit Lavenier (kimamila)
* Mobile et Ordinateur ?
* Interface proche de Cesium v1 ?

---
layout: two-cols
---

# Changement délais

## Ğ1 v1

* Un bloc toutes les 5 minutes environ.
* Transaction en blockchain en 10-15 min.
* Finalisation probabiliste, forks toujours possible (très impropable au bout de quelques heures).

<br>

# Changement capacités

* ~ 100 transferts par block 

⇒ < 100 transfers / 5min !

::right::

<h1 color="#fff">_</h1>

## Ğ1 v2

* Un bloc toutes les 6 secondes précisément.
* Transaction en blockchain en 5 à 10 secondes.
* Finalisation absolue, fork impossible après finalisation (~30 s quand le réseau va bien).

<br>

<h1 color="#fff">_</h1>

* 300 transferts par bloc

⇒ = 15 000 transfers / 5min !

---
layout: two-cols
---

# Changement certifications

## Ğ1 v1

* On peut émettre plusieurs certif en même temps.
* Les certifications restent en « piscine » jusqu'à leur validation définitive (une tous les 5 jours) ou expiration (2 mois).

⇒ Problèmes de synchro (car certifs pas en blockchain)

⇒ Problèmes de disponibilité des certif qui retardent les nouveaux entrants si trop de certif émises

::right::

<h1 color="#fff">_</h1>

## Ğ1 v2

* On ne peut émettre une certification que si la précédente à été émise il y a 5 jours ou plus.
* Disparition des « piscines », toute certification est inscrite dans la blockchain dès son émission.

⇒ Plus de prob. de synchro, on voit les mêmes certif partout (car en blockchain)

⇒ Plus de notion de « disponibilité », toute certif émise est instantannément validée et en blockchain.

---
hideInToc: true
---

# ✎

---
layout: two-cols
---

# Changement identités

## Ğ1 v1

* On peut transformer n'importe quel compte et compte membre (=créer son identité).
* L'identité reste en « piscine » jusqu'à sa validation définitive (5 cert dispo + distance) ou expiration (2 mois).

⇒ problèmes de synchro (car identité pas en blockchain), selon le noeud vous ne pouvez pas certifier la personne. 

::right::

<h1 color="#fff">_</h1>

## Ğ1 v2

* L'identité doit être créée par le 1er certificateur.
* Puis l'utilisateur doit confirmer sa création d'identité.
* Puis d'autres membres peuvent le certifier.
* L'identité passe au statut "validée" dès que les conditions sont réunies (5 certif + distance) ou est supprimée au bout de 2 mois.

⇒ Tout le monde voit la même identité partout (car en blockchain)


---

# Nouvelle fonctionnalités

## Changer la clé publique de son compte membre

<h3 color="#fff">_</h3>

### Ğ1 v1

* Il faut révoquer son compte membre et en créer un nouveau ➮ on perd toutes ses certifications.

<h3 color="#fff">_</h3>

### Ğ1 v2

* On pourra modifier sa clé publique en conservant ses certifications.
* Pour que ce soit sécurisé : l'ancienne clé publique pourra toujours révoquer l'identité pendant quelques mois.

---
hideInToc: true
---

# Nouvelle fonctionnalités

## Multi-signature onchain

<h3 color="#fff">_</h3>

### Ğ1 v1

* Possible, mais les signatures doivent être réunies offchain.

⇒ Nécessite de se réunir physiquement ou de partager un fichier, trop technique.

<h3 color="#fff">_</h3>

### Ğ1 v2

* Chaque co-signataire peut publier seul sa signature en blockchain sans avoir besoin de se synchroniser avec les autres ou de partager un ficher.
* On définit une liste de clés publiques et un seuil, par exemple 3 parmi 5.

⇒ Permettra de créer et utiliser un compte collectif sans compétences techniques (à condition qu'une interface l'expose).


---
hideInToc: true
---

# Nouvelle fonctionnalités

## Compte délégué

<h3 color="#fff">_</h3>

### Ğ1 v1

* Impossible.

<h3 color="#fff">_</h3>

### Ğ1 v2

* On pourra donner certains droits sur notre compte à un autre compte (délégataire), comme le droit de transférer de la monnaie.
* On pourra optionellement imposer un délai au délégataire, délai pendant lequel on peut "annuler" l'action.
* Cas d'usages possibles: 
  * Une asso peut nommer son trésorier comme délégataire avec un délai d'une semaine (ou autre). 
  * Un membre peut donner accès à ses DU à un simple portefeuille sur son mobile.


---
hideInToc: true
---

# Nouvelle fonctionnalités

## Autorisation de prélèvement

<h3 color="#fff">_</h3>

### Ğ1 v1

* Impossible.

<h3 color="#fff">_</h3>

### Ğ1 v2

* On pourra autoriser le versement d'une somme précise chaque mois* vers un destinataire précis.
* *Période paramétrable : 1 mois, 3 mois, 1 semaine, 1 jour, ce qu'on veut.
* Le montant pourra être exprimé en Ğ1 ou en DUĞ1 (automatiquement réévalué).
* N'importe qui pourra « exécuter » le prélèvement quand une « période » est écoulée.
* Chaque autorisation sera révocable sans conditions ni délai.

⇒ Au plus tard quelques mois après le passage à la v2.

---
hideInToc: true
---

# ✎

---

# Plan de migration

* ✅ Développer une 1ère PoC.
* ✅ lancer une 1ère monnaie de test pour les développeurs (ĞDev).
* ✅ 1ère batterie de tests sur la ĞDev.
* ☐ Étalonner les « poids » ( en cours…).
* ☐ Ajouter les fonctionnalités manquantes.
* ☐ 2ème batterie de tests sur la ĞDev (avec tests de charge).
* ☐ Coder de quoi convertir DB duniter V1 en genesis state Duniter v2.
* ☐ Tester la migration à blanc.
* ☐ Lancer la 2ème monnaie de test (ĞTest) basée sur l'état de la ğ1-test où de la Ğ1.
* ☐ 1ère batterie de tests sur la ĞTest (avec tests de charge).
* ☐ Attendre que les nouvelles interfaces soit prètes.
* ☐ Lancer la « Ğ1-mirror », une copie temporaire de la Ğ1.
* ☐ Campagne de tests utilisateurs sur la « Ğ1-mirror ».
* ☐ Intégrer les retours de cette campagne.
* ☐ Annonce d'une date officielle de migration de la prod.


---
class: text-center
---

# Questions/Réponses 

Merci de votre attention.
